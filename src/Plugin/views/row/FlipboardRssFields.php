<?php

namespace Drupal\views_flipboard\Plugin\views\row;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\row\RssFields;

/**
 * Renders an Flipboard RSS item based on fields.
 *
 * @ViewsRow(
 *   id = "flipboard_rss_fields",
 *   title = @Translation("Flipboard RSS fields"),
 *   help = @Translation("Display fields as Flipboard RSS items."),
 *   theme = "views_view_row_flipboard_rss",
 *   display_types = {"feed"}
 * )
 */
class FlipboardRssFields extends RssFields {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['enclosure_field'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $initial_labels = ['' => $this->t('- None -')];
    $view_fields_labels = $this->displayHandler->getFieldLabels();
    $view_fields_labels = array_merge($initial_labels, $view_fields_labels);

    $form['enclosure_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Enclosure field'),
      '#description' => $this->t('The field that is going to be used as the RSS item media for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['enclosure_field'],
      '#required' => TRUE,
    ];
    $form['category_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Category field'),
      '#description' => $this->t('The field that is going to be used as the RSS item category for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['category_field'],
      '#required' => FALSE,
    ];
    $form['query_string'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Extra query parameters'),
      '#description' => $this->t("A string that will be url encoded and appended to all items' link fields."),
      '#default_value' => $this->options['query_string'],
      '#required' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $errors = parent::validate();
    $required_options = [
      'title_field',
      'link_field',
      'description_field',
      'creator_field',
      'date_field',
      'enclosure_field',
    ];
    foreach ($required_options as $required_option) {
      if (empty($this->options[$required_option])) {
        $errors[] = $this->t('Row style plugin requires specifying which views fields to use for RSS item.');
        break;
      }
    }
    // Once more for guid.
    if (empty($this->options['guid_field_options']['guid_field'])) {
      $errors[] = $this->t('Row style plugin requires specifying which views fields to use for RSS item.');
    }
    return $errors;
  }

  /**
   * {@inheritdoc}
   */
  public function render($row) {
    static $row_index;
    if (!isset($row_index)) {
      $row_index = 0;
    }
    if (function_exists('rdf_get_namespaces')) {
      // Merge RDF namespaces in the XML namespaces in case they are used
      // further in the RSS content.
      $xml_rdf_namespaces = [];
      foreach (rdf_get_namespaces() as $prefix => $uri) {
        $xml_rdf_namespaces['xmlns:' . $prefix] = $uri;
      }
    }

    // Create the RSS item object.
    $item = new \stdClass();
    $item->title = $this->getField($row_index, $this->options['title_field']);
    $item->link = $this->getAbsoluteUrl($this->getField($row_index, $this->options['link_field']));
    if ($this->options['query_string']) {
      $item->link .= '?' . $this->options['query_string'];
    }
    $field = $this->getField($row_index, $this->options['description_field']);
    $item->description = is_array($field) ? $field : ['#markup' => $field];
    $item->enclosure = $this->getField($row_index, $this->options['enclosure_field']);

    $item->elements = [
      [
        'key' => 'pubDate',
        'value' => str_replace(["\r", "\n"], '', strip_tags($this->getField($row_index, $this->options['date_field']))),
      ],
      [
        'key' => 'dc:creator',
        'value' => $this->getField($row_index, $this->options['creator_field']),
        'namespace' => ['xmlns:dc' => 'http://purl.org/dc/elements/1.1/'],
      ],
      [
        'key' => 'category',
        'value' => $this->getField($row_index, $this->options['category_field']),
      ],
    ];
    $guid_is_permalink_string = 'false';
    $item_guid = $this->getField($row_index, $this->options['guid_field_options']['guid_field']);
    if ($this->options['guid_field_options']['guid_field_is_permalink']) {
      $guid_is_permalink_string = 'true';
      $item_guid = $this->getAbsoluteUrl($item_guid);
    }
    $item->elements[] = [
      'key' => 'guid',
      'value' => $item_guid,
      'attributes' => ['isPermaLink' => $guid_is_permalink_string],
    ];

    $row_index++;

    return [
      '#theme' => $this->themeFunctions(),
      '#view' => $this->view,
      '#options' => $this->options,
      '#row' => $item,
      '#field_alias' => $this->field_alias ?? '',
    ];
  }

}
